"use strict"

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Дану конструкцію доречно використовувати, коли ми отримуємо дані ззовні (від користувача, серверу, бази даних і т.д.).
// Приклади: 

// під час роботи з базою даних можуть виникати помилки, наприклад, якщо з'єднання з базою даних не встановлене, запит не правильний, таблиця не існує;

// при отриманні вхідних даних від користувача можуть виникати помилки, якщо користувач вводить неправильні дані. У цьому випадку можна використовувати try...catch, щоб перехопити ці помилки;


// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
const root = document.querySelector("#root");

const obligatedProp = [ "price", "author", "name"];

class BooksList {
  constructor(array) {
    this.array = array;
  }

  createList() {
    const list = document.createElement("ul");

    this.array.forEach(element => {
      const listItem = document.createElement("li");
      let isValid = true;

      obligatedProp.forEach(prop => {
        if (!element.hasOwnProperty(prop)) {
          console.error(`Властивість '${prop}' відсутня в об'єкті ${JSON.stringify(element)}`);
          isValid = false;
        }
      });

      if (isValid) {
        Object.entries(element).forEach(([key, value]) => {
          const propItem = document.createElement("p");
          propItem.textContent = `${key}: ${value}; `;
          listItem.append(propItem);
        });
        list.append(listItem);
      }
    });

    root.append(list);
  }
}

const bookList = new BooksList(books);
bookList.createList();





//  class BooksList {

//         constructor (array) {
//             this.array = array;
//         }
//      createList = function () {

//        const list = document.createElement("ul");
//        root.append(list);

//        this.array.forEach(element => { 
//         const listItem = document.createElement("li");
//         try {
//             if (element.name && element.author && element.price) {
//                     listItem.textContent =`автор ${element.author}, назва: ${element.name}, ціна:${element.price}` ;
//                 list.append(listItem);
//             }
//             if ( !element.name ) {
//                 throw new Error ("Property name is undefined") 
//             }
//             if ( !element.author ) {
//                 throw new Error ("Property name is undefined") 
//             }
//             if ( !element.price ) {
//                 throw new Error ("Property price is undefined") 
//             }
            
//         } catch (error) {
//             console.log(element);
//             console.log( error.message);
//         }

//        });
//     }
 
//  }

//  const booksList = new BooksList(books);
// bookList.createList()

 